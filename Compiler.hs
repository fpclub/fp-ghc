{-# LANGUAGE RecursiveDo #-}
module Compiler where

import Control.Monad.RWS
import Data.Void (Void)
import Data.Word

import ASM

-- |Mnemonic constants
[up, right, down, left] = map Const [0 .. 3]
[standard, fright, invisible] = map Const [0 .. 2]
[wall, empty, pill, powerPill, fruit, playerStart, ghostStart] = map Const [0 .. 6]

-- |Compiler types --------------------------------------

data Env = Env
    { freeVar    :: Word8
    , freeReg    :: Word8
    , instrCount :: Line
    }
    deriving Show

emptyEnv = Env 100 2 0

type Compiler = RWS Void [Command] Env

-- |Run a compiler, and get an assembler listing
compile :: Compiler () -> [Command]
compile c = listing where
    (_, listing) = execRWS c undefined emptyEnv

-- |Write a single instruction
write :: Command -> Compiler ()
write com = do
    tell [com]
    env@Env {instrCount = ic} <- get
    put $ env {instrCount = succ ic}

-- |Memory and variables ----------------------------------
class Valued a where
    loadValue :: a -> Compiler Arg

instance Valued Arg where
    loadValue v = return v
    
type Var = Arg
type Ptr = Arg

malloc :: Compiler Var
malloc = do
    env@Env {freeVar = c} <- get
    put $ env {freeVar = c + 1}
    return $ Mem c

ralloc :: Compiler Ptr
ralloc = do
    env@Env {freeReg = c} <- get
    put $ env {freeReg = c + 1}
    return $ AReg $ toEnum $ fromIntegral c

ld :: Var -> Expr
ld = GetArg

peek :: Ptr -> Var
peek (AReg r) = (AIReg r)

var :: Expr -> Compiler Var
var expr = do
    v <- malloc
    v .= expr
    return v

ptr :: Expr -> Compiler Ptr
ptr expr = do
    r <- ralloc
    r .= expr
    return r

-- |Increment and decrement
inc, dec :: Var -> Compiler ()
inc v = write $ Un INC v
dec v = write $ Un DEC v

-- |GOTO, jumps, loops
label :: Compiler Line
label = instrCount `liftM` get

goto :: Line -> Compiler ()
goto line = write $ Bin MOV (AReg PC) (Const line)

orderingToJump cond = case cond of
                        LT -> JLT
                        EQ -> JEQ
                        GT -> JGT

if_ :: Var -> Ordering -> Expr -> Compiler () -> Compiler () -> Compiler ()
if_ v cond e tBody fBody = mdo
    c <- var e
    write $ Jmp (orderingToJump cond) tLabel v c
    fBody
    goto end
    tLabel <- label
    tBody
    end <- label
    return ()

when_ :: Var -> Ordering -> Expr -> Compiler () -> Compiler ()
when_ v cond e tBody = if_ v cond e tBody (return ())

while :: Var -> Ordering -> Expr -> Compiler () -> Compiler ()
while v cond e body = mdo
    start <- label
    c <- var e
    write $ Jmp (orderingToJump cond) middle v c
    goto end
    middle <- label
    body
    goto start
    end <- label
    return ()

for :: Var -> Expr -> Expr -> Compiler () -> Compiler ()
for v start end body = do
    v .= start
    while v LT end body

halt :: Compiler ()
halt = write HLT

-- |Arithmetic expressions
data Expr
    = GetArg Arg
    | BinExpr Binary Expr Expr

instance Num Expr where
    fromInteger c = GetArg $ Const $ fromInteger c
    (+) = BinExpr ADD
    (-) = BinExpr SUB
    (*) = BinExpr MUL
    abs = undefined
    signum = undefined

(./) = BinExpr DIV
infixl 7 ./

(.&) = BinExpr AND
infixl 7 .&

(.|) = BinExpr OR
infixl 7 .|

(.^) = BinExpr XOR
infixl 6 .^

-- |Assignment
(.=) :: Var -> Expr -> Compiler ()
(.=) v expr = case expr of
    GetArg arg -> write $ Bin MOV v arg
    BinExpr op e1 e2 -> do
        v .= e1
        a2 <- var e2
        write $ Bin op v a2
infix 1 .=

-- |Interrupts
go :: Expr -> Compiler ()
go dir = do
    AReg A .= dir
    write $ INT 0

getPlayer :: Compiler (Var, Var)
getPlayer = do
    write $ INT 1
    x <- var (ld $ AReg A)
    y <- var (ld $ AReg B)
    return (x, y)

getId :: Compiler Var
getId = do
    write $ INT 3
    var $ ld $ AReg A

getCoords :: Var -> Compiler (Var, Var)
getCoords v = do
    write $ Bin MOV (AReg A) v
    write $ INT 5
    x <- var (ld $ AReg A)
    y <- var (ld $ AReg B)
    return (x, y)

getVitDir :: Var -> Compiler (Var, Var)
getVitDir v = do
    write $ Bin MOV (AReg A) v
    write $ INT 6
    x <- var (ld $ AReg A)
    y <- var (ld $ AReg B)
    return (x, y)

getCell :: (Expr, Expr) -> Compiler Var
getCell (x, y) = do
    AReg A .= x
    AReg B .= y
    write $ INT 7
    var $ ld $ AReg A

debug :: Compiler ()
debug = write $ INT 8

-- |Test programs
test = do
    x <- var 42
    y <- malloc
    y .= ld x + 2
    halt

test2 = do
    id <- getId
    (x, y) <- getCoords id
    go (ld x + ld y)

test3 = do
    x <- var 66
    begin <- label
    goto begin
    halt

test4 = mdo    
    goto end
    x <- var 66
    end <- label
    halt

test5 = do
    x <- var 42
    while x GT 0 $ do
        x .= ld x - 2
    halt

deref = ld . peek

testFickle = do
    a <- var 255
    b <- ptr 0
    c <- ptr 0
    while c LT 4 $ do
        when_ a GT (deref c) $ do
            a .= deref c
            b .= ld c
        inc c
    go (ld b)
    id <- getId
    (_, dir) <- getVitDir id
    b .= ld dir
    inc (peek b)
    when_ id EQ 0 debug
    halt

-- |Prettyprint
showListing :: [Command] -> String
showListing = unlines . map show

testShow = putStrLn . showListing . compile
