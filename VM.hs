module VM where

import ASM
import Control.Monad
import Control.Monad.ST
import Data.Bits
import qualified Data.ByteString as BS
import Data.Char (ord)
import Data.Array
import Data.Array.ST
import Data.Word
import Data.STRef
import Prelude hiding (read)

type Point = (Word8, Word8)
data Field = Field
    { ghostIndex :: Word8
    , level      :: Array Point Word8
    , ghostStart :: Array Word8 Point
    , ghostPos   :: Array Word8 Point
    , ghostVit   :: Array Word8 Word8
    , lmanStart  :: Point
    , lmanPos    :: Point
    }

emptyBorders = ((0, 0), (0, 0))

emptyField = Field
    0
    (listArray emptyBorders [0])
    (listArray (0, 0) [(0, 0)])
    (listArray (0, 0) [(0, 0)])
    (listArray (0, 0) [0])
    (0, 0)
    (0, 0)


data VM s = VM
    { code      :: Array Word8 Command
    , memory    :: STUArray s Word8 Word8
    , registers :: STUArray s Reg   Word8
    , tickCount :: STRef s Int
    , vmField   :: Field
    , ghostDir  :: STRef s Word8
    }

{-instance Show VM where
    show VM {registers = reg} =
        unwords $ map (\() -}

initVM code field dir = do
    mem <- newArray (0, 255) 0
    reg <- newArray (A, PC) 0
    tc  <- newSTRef 0
    ghostDir <- newSTRef dir
    return $ VM code mem reg tc field ghostDir

readVM VM {memory = mem, registers = reg} src = case src of
    (AReg r) -> readArray reg r 
    (AIReg r) -> do
        addr <- readArray reg r
        readArray mem addr
    (Mem val) -> do
        addr <- readArray mem val
        readArray mem addr
    (Const val) -> return val
    --_ -> error "Wrong addressing"

writeVM VM {memory = mem, registers = reg} dst val = case dst of
    (AReg r) -> writeArray reg r val
    (AIReg r) -> do
        addr <- readArray reg r
        writeArray mem addr val
    (Mem val) -> do
        addr <- readArray mem val
        writeArray mem addr val
    _ -> error "Wrong addressing"

funOp MOV = flip const
funOp ADD = (+)
funOp SUB = (-)
funOp MUL = (*)
funOp DIV = quot
funOp AND = (.&.)
funOp OR  = (.|.)
funOp XOR = xor

funUn INC = succ
funUn DEC = pred

funCond JLT = (<)
funCond JEQ = (==)
funCond JGT = (>)

step vm@(VM code mem reg tc field dir) = do
    let read  = readVM vm
        write = writeVM vm
        writeAB (a, b) = write (AReg A) a
                      >> write (AReg B) b
    pc <- read (AReg PC)
    let oldPC = pc
    let com = code ! pc
    case com of
        Un op dst -> do
            val <- read dst
            write dst $ funUn op val
        Bin op dst src -> do
            v1 <- read src
            v2 <- read dst
            write dst $ funOp op v2 v1
        INT 0 -> read (AReg A) >>= writeSTRef dir 
        INT 1 -> writeAB (lmanPos field)
        INT 2 -> return () --TODO (The second L-man)
        INT 3 -> write (AReg A) $ ghostIndex field
        INT 4 -> read (AReg A) >>= writeAB . (ghostStart field !)
        INT 5 -> read (AReg A) >>= writeAB . (ghostPos field !)
        INT 6 -> read (AReg A) >>= write (AReg A) . (ghostVit field !)
        INT 7 -> do
            x <- read (AReg A)
            y <- read (AReg B)
            write (AReg A) $ level field ! (x, y)
        INT 8 -> return () --TODO?
        Jmp cond loc x y -> do
            v1 <- read x
            v2 <- read y
            when (funCond cond v1 v2) (write (AReg PC) loc)
        HLT -> writeSTRef tc 1024 --Hackery
    newPC <- read (AReg PC)
    when (newPC == oldPC) (write (AReg PC) (newPC + 1))
    ticks <- readSTRef tc
    if ticks < 1024
        then do
            writeSTRef tc (succ ticks)
            step vm
        else readSTRef dir >>= return
    
testMiner =
    [ Bin MOV (AReg A) (Const 2)
    , INT 0
    , HLT
    ]

testFlipper =
    [ INT 3
    , INT 5
    , Bin AND (AReg A) (Const 1)
    , Bin MOV (AReg B) (AReg A)
    , Bin MOV (AReg A) (Const 2)
    , Jmp JEQ 7 (AReg B) (Const 1)
    , Bin MOV (AReg A) (Const 0)
    , INT 0
    , HLT
    ]

runTest codelist dir = runST $ do
    let code = listArray (0, fromIntegral (length codelist) - 1) codelist
    vm <- initVM code emptyField dir
    step vm
