{-# LANGUAGE RecursiveDo #-}

module ASM where

--import Control.Monad.Trans.State
import Control.Monad.RWS
import Data.Ix
import qualified Data.Map as M
import Data.Void (Void)
import Data.Word
import Text.Printf

data Command
    = Un Unary Arg
    | Bin Binary Arg Arg
    | Jmp Jump Line Arg Arg
    | INT Val
    | HLT

data Unary  = INC | DEC
    deriving Show
data Binary = MOV | ADD | SUB | MUL | DIV | AND | OR | XOR
    deriving Show
data Jump   = JLT | JEQ | JGT
    deriving Show

type Line = Word8

instance Show Command where
    show (Un un arg) = printf "%s %s" (show un) (show arg)
    show (Bin bin arg1 arg2) = printf "%s %s,%s" (show bin) (show arg1) (show arg2)
    show (Jmp jmp line arg1 arg2) = printf "%s %d,%s,%s" (show jmp) line (show arg1) (show arg2)
    show (INT val) = printf "INT %d" val
    show HLT = "HLT"

data Arg = AReg Reg | AIReg Reg | Const Val | Mem Val
instance Show Arg where
    show (AReg reg) = show reg
    show (AIReg reg) = printf "[%s]" (show reg)
    show (Const val) = show val
    show (Mem val) = printf "[%d]" val

data Reg = A | B | C | D | E | F | G | H | PC
    deriving (Show, Eq, Enum, Ord, Ix)

type Val = Word8

